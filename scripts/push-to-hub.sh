#!/bin/sh

export REPO="toreskog/webstore-api"

#Remove old image
docker rmi $REPO:latest

# Build Dokcer image
docker build -t $REPO:latest .
docker tag $REPO:latest $REPO:latest

# Push to dockerhub
docker login -u $DOCKER_USERNAME -p $DOCKER_PASSWORD
docker push $REPO:latest