#!/bin/sh

# Login
ssh root@138.68.97.139

#Remove image
docker stop webstore-api
docker rm webstore-api
docker rmi toreskog/webstore-api

#Upload start new image
docker login -u $DOCKER_USERNAME -p $DOCKER_PASSWORD
docker run -p 5400:5400 -d --restart=always --name webstore-api \
-e MONGO_USER=$MONGO_USER \
-e MONGO_PASSWORD=$MONGO_PASSWORD \
-e MONGO_PORT_27017_TCP_ADDR=$MONGO_PORT_27017_TCP_ADDR \
toreskog/webstore-api
