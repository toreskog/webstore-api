FROM golang:alpine

#Install git
RUN apk update && \
    apk upgrade && \
    apk add git

WORKDIR /go/src/app

COPY . /go/src/app

RUN go get ./...
RUN go build ./...

ENTRYPOINT /go/bin/app

EXPOSE 5400