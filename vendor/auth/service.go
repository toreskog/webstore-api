package auth

import (
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"time"
	"shared/database"
	"gopkg.in/mgo.v2/bson"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2"
)

var (
	key = []byte("f32908gfr894uer894ue36t72tk9834ur78")
)

func initDB() error {
	session := database.Session.Copy()
	defer session.Close()
	c := session.DB(database.Name).C("users")

	index := mgo.Index{
		Key: []string{"username"},
		Unique: true,
		DropDups:true,
		Sparse:true,
	}

	err := c.EnsureIndex(index)
	if err != nil {
		return err
	}
	return nil
}

func createToken(cred UserCredentials) (string, error) {
	user, _ := findUserByUsername(cred.Username)
	if user != nil {
		err := bcrypt.CompareHashAndPassword(user.Hash, []byte(cred.Password))
		if err != nil {
			return "", errors.New("Unauthorized")
		}

		token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"userID": user.ID,
			"username": user.Username,
			"exp":  time.Now().Add(time.Hour * 1).Unix(),
			"iat":  time.Now().Unix(),
		})

		tokenString, err := token.SignedString(key)
		if err != nil {
			return "", err
		}

		return tokenString, nil
	}

	return "", errors.New("Unauthorized")
}

func validateToken(tokenString string) (*jwt.MapClaims, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return key, nil
	})
	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {
		return &claims, nil
	}

	return nil, err
}

func createUser(user *User) error {
	var err error

	session := database.Session.Copy()
	defer session.Close()
	c := session.DB(database.Name).C("users")

	user.ID = bson.NewObjectId()
	user.Hash, err = bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	err = c.Insert(user)
	if err != nil {
		return err
	}

	return nil
}

func findUserByUsername(username string) (*User, error) {
	session := database.Session.Copy()
	defer session.Close()

	c := session.DB(database.Name).C("users")

	var user User

	err := c.Find(bson.M{"username": username}).One(&user)
	if err != nil {
		return nil, err
	}

	return &user, nil
}
