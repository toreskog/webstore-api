package auth

import "gopkg.in/mgo.v2/bson"

var (
	RoleAdmin = "admin"

	ActivityProtected = "protected"
)

type UserCredentials struct {
	Username string        `json:"username"`
	Password string        `json:"password"`
}

type User struct {
	ID       bson.ObjectId `json:"id" bson:"_id"`
	Username string        `json:"username" bson:"username"`
	Password string        `json:"password" bson:"-"`
	Hash     []byte        `json:"-" bson:"hash"`
	Roles    []string      `json:"roles" bson:"roles"`
}