package auth

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
	"shared/response"
)

func Init(r *gin.Engine) error {
	err := initDB()
	if err != nil {
		return err
	}

	r.POST("/login", login)
	r.GET("/claims", claims)
	r.POST("/users", postUser)

	return nil
}

func login(ctx *gin.Context) {
	var cred UserCredentials
	ctx.BindJSON(&cred)

	token, err := createToken(cred)
	if err != nil {
		ctx.JSON(http.StatusUnauthorized, gin.H{
			"error": err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"token": token,
	})
}

func postUser(ctx *gin.Context) {
	var err error
	var user User

	err = ctx.BindJSON(&user)
	if err != nil {
		response.BadRequest(ctx, err.Error())
		return
	}

	err = createUser(&user)
	if err != nil {
		response.BadRequest(ctx, err.Error())
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"user": user,
	})
}


func claims(ctx *gin.Context) {
	var tokenString string
	authHeader := ctx.Request.Header["Authorization"]

	if len(authHeader) > 0 && strings.HasPrefix(authHeader[0], "Bearer ") {
		tokenString = strings.TrimPrefix(authHeader[0], "Bearer ")
	}

	claims, err := validateToken(tokenString)
	if err != nil {
		ctx.JSON(http.StatusUnauthorized, gin.H{
			"error": err.Error(),
		})
	}

	ctx.JSON(http.StatusOK, gin.H{
		"claims": claims,
	})
}
