package order

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"net/http"
	"shared/response"
)

func Init(r *gin.Engine) {
	r.GET("/orders", getOrder)
	r.POST("/orders", postOrder)
	r.GET("/order-lines/:cart", getOrderLines)
}

func getOrder(ctx *gin.Context) {
	orders, err := readOrders()
	if err != nil {
		response.BadRequest(ctx, err.Error())
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"Order": orders,
	})
}

func postOrder(ctx *gin.Context) {
	var order Order
	var err error

	err = ctx.BindJSON(&order)
	if err != nil {
		response.BadRequest(ctx, err.Error())
		return
	}

	err = createOrderLines(order.OrderLines, &order)
	if err != nil {
		response.BadRequest(ctx, err.Error())
		return
	}

	charge, err := chargeCard(uint64(order.TotalPrice*100), order.StripeToken, "Example Charge")
	if err != nil {
		stripeErr(ctx, err)
		return
	}

	err = createOrder(order)
	if err != nil {
		response.BadRequest(ctx, err.Error())
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"charge": charge,
	})
}

func getOrderLines(ctx *gin.Context) {
	var order Order
	var lines []OrderLine

	c := []byte(ctx.Param("cart"))

	err := json.Unmarshal(c, &lines)
	if err != nil {
		response.BadRequest(ctx, err.Error())
		return
	}

	err = createOrderLines(lines, &order)
	if err != nil {
		response.BadRequest(ctx, err.Error())
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"order": order,
	})
}
