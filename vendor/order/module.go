package order

import (
	"gopkg.in/mgo.v2/bson"
)

type Customer struct {
	Name    string  `json:"name" bson:"name"`
	Phone   string  `json:"phone" bson:"phone"`
	Email   string  `json:"email" bson:"email"`
	Address Address `json:"address" bson:"address"`
}

type Address struct {
	Street  string `json:"street" bson:"street"`
	ZipCode string `json:"zipCode" bson:"zipCode"`
	City    string `json:"city" bson:"city"`
}

type Order struct {
	Id          bson.ObjectId `json:"id" bson:"_id"`
	StripeToken string        `json:"stripeToken" bson:"stripeToken"`
	Customer    Customer      `json:"customer" bson:"customer"`
	OrderLines  []OrderLine   `json:"orderLines" bson:"orderLines"`
	TotalPrice  int           `json:"totalPrice" bson:"totalPrice"`
}

type OrderLine struct {
	ProductID bson.ObjectId `json:"productId" bson:"_id"`
	Name      string        `json:"name" bson:"name"`
	Quantity  int           `json:"quantity" bson:"quantity"`
	UnitPrice int           `json:"unitPrice" bson:"unitPrice"`
}

type CartItem struct {
	ProductID bson.ObjectId `json:"productId" bson:"_id"`
	Quantity  int           `json:"quantity" bson:"quantity"`
}
