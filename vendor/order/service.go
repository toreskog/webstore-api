package order

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/charge"
	"gopkg.in/mgo.v2/bson"
	"product"
	"shared/database"
)

func createOrder(order Order) error {
	order.Id = bson.NewObjectId()

	session := database.Session.Copy()
	defer session.Close()

	c := session.DB(database.Name).C("orders")

	err := c.Insert(order)
	return err
}

func createOrderLines(lines []OrderLine, order *Order) error {
	var total int = 0
	for i := range lines {
		product, err := product.FindOne(lines[i].ProductID)
		if err != nil {
			return err
		}
		lines[i].Name = product.Name
		lines[i].UnitPrice = product.Price

		total += lines[i].UnitPrice * lines[i].Quantity
	}

	order.OrderLines = lines
	order.TotalPrice = total

	return nil
}

func readOrders() ([]Order, error) {
	session := database.Session.Copy()
	defer session.Close()

	c := session.DB(database.Name).C("orders")

	var orders []Order

	err := c.Find(bson.M{}).All(&orders)

	return orders, err
}

func chargeCard(total uint64, token string, desc string) (*stripe.Charge, error) {
	stripe.Key = "sk_test_5ITG8DTSfj6Kvya9AooRDvya"

	params := &stripe.ChargeParams{
		Amount:   total,
		Currency: "nok",
		Desc:     desc,
	}
	params.SetSource(token)

	charge, err := charge.New(params)
	if err != nil {
		return nil, err
	}

	return charge, nil
}

func stripeErr(ctx *gin.Context, err error) {
	var e stripe.Error
	json.Unmarshal([]byte(err.Error()), &e)

	ctx.JSON(e.HTTPStatusCode, gin.H{
		"error": e,
	})
}
