package response

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func BadRequest(ctx *gin.Context, err string) {
	ctx.JSON(http.StatusBadRequest, gin.H{
		"error": err,
	})
}
