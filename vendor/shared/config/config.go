package config

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"shared/database"
)

func Read() *Config {
	file, err := ioutil.ReadFile("./config/config.json")
	if err != nil {
		log.Println(err)
		return nil
	}

	err = json.Unmarshal(file, config)
	if err != nil {
		log.Println(err)
		return nil
	}

	return config
}

// *****************************************************************************
// Application Settings
// *****************************************************************************

// config the settings variable
var config = &Config{}

// configuration contains the application settings
type Config struct {
	Database database.Info `json:"Database"`
}
