package database

import (
	"gopkg.in/mgo.v2"
	"log"
	"os"
	"strconv"
)

var (
	Session *mgo.Session
	Name    string
)

type Info struct {
	Name string
	Host string
	Port int
}

func Connect(d Info) {
	var err error
	Name = d.Name

	user := os.Getenv("MONGO_USER")
	pass := os.Getenv("MONGO_PASSWORD")

	dialInfo := &mgo.DialInfo{
		Addrs:    []string{d.Host + ":" + strconv.Itoa(d.Port)},
		Username: user,
		Password: pass,
	}

	Session, err = mgo.DialWithInfo(dialInfo)
	if err != nil {
		log.Println("MongoDB Driver Error", err)
	}
}
