package product

import (
	"gopkg.in/mgo.v2/bson"
	"shared/database"
)

func FindOne(id bson.ObjectId) (Product, error) {
	session := database.Session.Copy()
	defer session.Close()

	c := session.DB(database.Name).C("products")

	var product Product

	err := c.FindId(id).One(&product)

	return product, err
}

func FindAll(ids ...bson.ObjectId) ([]Product, error) {
	session := database.Session.Copy()
	defer session.Close()

	c := session.DB(database.Name).C("products")

	var product []Product

	query := bson.M{"_id": bson.M{"$in": ids}}
	err := c.Find(query).All(&product)

	return product, err
}
