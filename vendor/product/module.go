package product

import "gopkg.in/mgo.v2/bson"

type Product struct {
	Id       bson.ObjectId `json:"id" bson:"_id"`
	Name     string        `json:"name" bson:"name"`
	Price    int           `json:"price" bson:"price"`
	Image    string        `json:"image" bson:"image"`
	Category string        `json:"category" bson:"category"`
	Quantity int           `json:"quantity" bson:"quantity"`
}
