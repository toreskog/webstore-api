package product

import (
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
	"net/http"
	"shared/database"
	"shared/response"
	"strings"
)

func Init(r *gin.Engine) {
	r.GET("/products", getAllProducts)
	r.GET("/products/:ids", getProducts)
	r.POST("/products", createProducts)
	r.DELETE("/products/:id", delete)
}

func getAllProducts(ctx *gin.Context) {
	var err error

	session := database.Session.Copy()
	defer session.Close()

	c := session.DB(database.Name).C("products")

	var products []Product

	err = c.Find(bson.M{}).All(&products)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"products": products,
	})
}

func getProducts(ctx *gin.Context) {
	var err error
	ids := strings.Split(ctx.Param("ids"), ",")

	oids := make([]bson.ObjectId, len(ids))
	for i := range ids {
		if !bson.IsObjectIdHex(ids[i]) {
			response.BadRequest(ctx, "Not valid id")
			return
		}
		oids[i] = bson.ObjectIdHex(ids[i])
	}

	product, err := FindAll(oids...)
	if err != nil {
		response.BadRequest(ctx, err.Error())
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"products": product,
	})
}

func createProducts(ctx *gin.Context) {
	var err error
	var products []Product

	err = ctx.BindJSON(&products)
	if err != nil {
		response.BadRequest(ctx, err.Error())
		return
	}

	productSlice := make([]interface{}, len(products))
	for i, p := range products {
		p.Id = bson.NewObjectId()
		productSlice[i] = p
	}

	session := database.Session.Copy()
	defer session.Close()

	c := session.DB(database.Name).C("products")

	err = c.Insert(productSlice...)
	if err != nil {
		response.BadRequest(ctx, err.Error())
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"posted elements": productSlice,
	})
}

func delete(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "DELETE not implemented",
	})
}
