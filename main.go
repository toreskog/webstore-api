package main

import (
	"github.com/gin-gonic/gin"

	"auth"
	"order"
	"product"
	"shared/config"
	"shared/database"
)

func cors() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Writer.Header().Add("Access-Control-Allow-Origin", "*")
		ctx.Writer.Header().Set("Access-Control-Max-Age", "86400")
		ctx.Writer.Header().Set("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE")
		ctx.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Authorization, Origin")
	}
}

func main() {
	c := config.Read()

	r := gin.Default()

	r.Use(cors())

	database.Connect(c.Database)

	product.Init(r)
	order.Init(r)
	auth.Init(r)

	r.Run(":5400")
}
